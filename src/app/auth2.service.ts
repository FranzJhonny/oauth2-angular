import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Auth2Service {
  public clientId = '961e837d-5648-4bc5-bd61-15294ef81fad';
  public redirectUri = 'http://localhost:2400/';
  public secret = 'hSgt4DttXetggJGAILZZgwu6kRLuVmJW7BzUjjVg'
  constructor(private _http: HttpClient) { }

  retrieveToken(code) {
    let params = new URLSearchParams();   
    params.append('grant_type','password');
    params.append('client_id', this.clientId);
    params.append('client_secret', this.secret);
    params.append('username','admin@example.org');
    params.append('password','admin');

    let headers = 
      new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'});
       
      this._http.post('http://154.53.63.81/oauth/token', 
        params.toString(), { headers: headers })
        .subscribe(
          data => {
            console.log(data);
          },
          err => alert('Invalid Credentials')); 
  }
}
