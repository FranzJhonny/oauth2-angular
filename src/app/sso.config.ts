import { AuthConfig } from 'angular-oauth2-oidc';

export const authConfig = {

  // Url of the Identity Provider
  issuer: 'http://154.53.63.81/oauth/token',

  // URL of the SPA to redirect the user to after login
  redirectUri: 'window.location.origin',
 /*  redirectUri: window.location.origin , */
  /*  responseType:'code', */
   postLogoutRedirectUri:'window.location.origin',
   dummyClientSecret: 'evISv1K7SDCFoySX5bbLBozUA5NvZ_0MaFYgLZaWKo0CT-Vnxr_IUJDJbxnCMlNf4DmnKGszFejFThgD9z_bAA',
  // The SPA's id. The SPA is registerd with this id at the auth-server
   clientId: '961e837d-5648-4bc5-bd61-15294ef81fad',
  // set the scope for the permissions the client should request
  // The first three are defined by OIDC. The 4th is a usecase-specific one
  scope: 'openid profile email',
  username: "admin@example.org",
  password:"admin"


}