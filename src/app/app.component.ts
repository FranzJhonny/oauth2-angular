import { Component } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './sso.config';
import { filter } from 'minimatch';
import { Auth2Service } from './auth2.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private oauthService:OAuthService, private _service: Auth2Service){
    this.configureSingleSignOn();
  }

  configureSingleSignOn(){
    this.oauthService.configure(authConfig);
    console.log('qw');
    
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    console.log(this.oauthService.tokenValidationHandler);
/*     
  let vad=  this.oauthService.loadDiscoveryDocumentAndTryLogin(); */
  }

  login(){
  /*  max geheim */

  var uaser ={
    "username": "admin@example.org",
     "password": "admin"
  } 
    this.oauthService.initImplicitFlowInternal('asd',{ "username": "admin@example.org", "password": "admin"})
  }

  logout(){
    this.oauthService.logOut();
  }

  get token(){
    let claims:any = this.oauthService.getIdentityClaims();
    console.log(claims);

    return claims ? claims : null;
  }

  login2(){
    let i = window.location.href.indexOf('code');
    this._service.retrieveToken(window.location.href.substring(i + 5));

  }
}
